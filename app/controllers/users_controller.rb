class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    set_user
    case @user.serverError
    when "205 partial content"
      respond_to do |format|
        format.html { render :show }
        format.any  { head :partial_content }
      end
    when "400 bad request"
      respond_to do |format|
        format.html { render :show }
        format.any  { head :bad_request }
      end
    when "403 forbidden"
      respond_to do |format|
        format.html { render :show }
        format.any  { head :forbidden}
      end
    when "404 not found"
      respond_to do |format|
        format.html { render :show }
        format.any  { head :not_found }
      end
    when "500 internal server error"
      respond_to do |format|
        format.html { render :show }
        format.any  { head :internal_server_error }
      end
    when "504 gateway timeout"
      respond_to do |format|
        format.html { render :show }
        format.any  { head :gateway_timeout }
      end
    when "garbage_text"
      respond_to do |format|
        format.html { render :show }
        format.json { render :html => 'test test test' }
      end
    when "garbage_json"
      respond_to do |format|
        format.html { render :show }
        format.json { render :json => { name: "junk", name2: "junk2" }}
      end
    when "garbage_html"
      respond_to do |format|
        format.html { render :show }
        format.json { render :html => '<html><body><h1>Can we break this (html edition)?</h1>I hope not...<h2>What if I embed quasi-expected json text now?</h2>{"current":{"version":"1.2.3"},"update":{"type":"trickle","progress":22,"finishEstimate":22,"userVisible":false,"version":"1.2.3","lang":"en"}}</body></html>'.html_safe }
      end
    else # includes "no error" case
      respond_to do |format|
        format.html { render :show }
        format.json { render :json => createJSON }
      end
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :updateAvailable, :finishestimate, :percentage, :status, :serverError)
    end

    def createJSON
      set_user
      {
        
        #Required
        :current => {
            # Required
            :version => '1.2.3'
        },
          
        # Optional. Only present if there are pending update.
        :update => {
          # Required
          :type => @user.status,
          
          # Required
          :progress => @user.percentage,
          
          # Optional, in minutes, may not have sufficient information
          :finishEstimate => @user.finishestimate,
          
          # Required. Used to implement the ‘grace period’ around
          # showing availability of update, in case trickle is taking
          # too long. This will be controlled by settings on the FW 
          # package upload page, can differ per firmware, 
          # and may support complex logic around time vs progress 
          :userVisible => @user.updateAvailable,
          :version => '1.2.3', 
          
          # Optional. Needed to support legacy i18n fwup logic on 
          # Dream Pony devices.
          :lang => 'en'
          }
          # more component types may be defined in the future on that level
      }
    end
end


