json.extract! user, :id, :name, :updateAvailable, :percentage, :created_at, :updated_at, :serverError
json.url user_url(user, format: :json)
