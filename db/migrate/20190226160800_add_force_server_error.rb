class AddForceServerError < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :serverError, :string
  end
end
